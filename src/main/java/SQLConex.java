import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLConex {

  private static final String URL = "jdbc:sqlserver://CAPTAINJKM\\SQLEXPRESS:49861;database=hospital;instance=SQLEXPRESS;";
  private static final String USUARIO = "ealfonso_bms";
  private static final String CONTRASENA = "bms044jk";

  public static Connection conexion = null;

  public Connection obtenerConexion() {
    try {

      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

      conexion = DriverManager.getConnection(URL, USUARIO, CONTRASENA);
      System.out.println("¡Conexión exitosa!");
    } catch (ClassNotFoundException e) {
      System.err.println("Error al cargar el controlador JDBC de SQL Server: " + e.getMessage());
    } catch (SQLException e) {
      System.err.println("Error al conectar con la base de datos: " + e.getMessage());
    }
    return conexion;
  }

  public static void cerrarConexion() {
    if (conexion != null) {
      try {
        conexion.close();
        System.out.println("¡Conexión cerrada!");
      } catch (SQLException e) {
        System.err.println("Error al cerrar la conexión: " + e.getMessage());
      }
    }
  }

}
