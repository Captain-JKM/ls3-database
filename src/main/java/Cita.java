public class Cita {
  private String IDCita;
  private String FechaHora;
  private String MotivoCita;
  private String EstadoCita;
  private String IDPaciente;
  private String IDMedico;

  public String getIDCita() {
    return IDCita;
  }
  // Hola
  // COmments
  // Tu amiga se fue
  // COn quien te estas quedando
  // sUENA SUE CANCION

  public void setIDCita(String IDCita) {
    this.IDCita = IDCita;
  }

  public String getFechaHora() {
    return FechaHora;
  }

  public void setFechaHora(String fechaHora) {
    FechaHora = fechaHora;
  }

  public String getMotivoCita() {
    return MotivoCita;
  }

  public void setMotivoCita(String motivoCita) {
    MotivoCita = motivoCita;
  }

  public String getEstadoCita() {
    return EstadoCita;
  }

  public void setEstadoCita(String estadoCita) {
    EstadoCita = estadoCita;
  }

  public String getIDPaciente() {
    return IDPaciente;
  }

  public void setIDPaciente(String IDPaciente) {
    this.IDPaciente = IDPaciente;
  }

  public String getIDMedico() {
    return IDMedico;
  }

  public void setIDMedico(String IDMedico) {
    this.IDMedico = IDMedico;
  }
}
