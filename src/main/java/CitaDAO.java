import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CitaDAO extends SQLConex {

  private Connection connection;
  public CitaDAO (Connection connection){
    this.connection = connection;
  }

  public void createCita(Cita cita) throws SQLException {
    String querySQL = "Insert INTO Cita( IDCita, FechaHora, MotivoCita, EstadoCita, IDPaciente, IDMedico )" +
            "VALUES (?,?,?,?,?,?)";
    try (PreparedStatement statement = connection.prepareStatement (querySQL)){
      statement.setString (1, cita.getIDCita ());
      statement.setString (2, cita.getFechaHora ());
      statement.setString (3, cita.getMotivoCita ());
      statement.setString (4, cita.getEstadoCita ());
      statement.setString (5, cita.getIDPaciente ());
      statement.setString (6, cita.getIDMedico ());

      statement.executeUpdate ();
    }
  }


  public List<Cita> obtenerTodasLasCitas() throws SQLException {
    List<Cita> citas = new ArrayList<>();

    try (Connection conexion = obtenerConexion()) {
      String query = "SELECT * FROM Cita";
      PreparedStatement statement = conexion.prepareStatement(query);
      ResultSet resultSet = statement.executeQuery();
      while (resultSet.next()) {
        Cita cita = new Cita();
        cita.setIDCita (resultSet.getString("IDCita"));
        cita.setEstadoCita (resultSet.getString("FechaHora"));
        cita.setMotivoCita ( resultSet.getString("MotivoCita"));
        cita.setEstadoCita ( resultSet.getString("EstadoCita"));
        cita.setIDPaciente ( resultSet.getString("IDPaciente"));
        cita.setIDMedico (resultSet.getString ("IDMedico"));


        citas.add(cita);
      }
    }

    return citas;
  }

  public void updateCita(String id, Cita cita) throws SQLException {
    String querySQL = "UPDATE Cita SET FechaHora=?, MotivoCita=?, EstadoCita=?, IDPaciente=?, IDMedico=? WHERE IDCita=?";

    try (PreparedStatement statement = connection.prepareStatement(querySQL)) {
      statement.setString(1, cita.getFechaHora());
      statement.setString(2, cita.getMotivoCita());
      statement.setString(3, cita.getEstadoCita());
      statement.setString(4, cita.getIDPaciente());
      statement.setString(5, cita.getIDMedico());
      statement.setString(6, id);

      statement.executeUpdate();
    }
  }
  //Previus
  


  public void deleteCita(String id) throws SQLException {
    String querySQL = "DELETE FROM Cita WHERE IDCita=?";

    try(PreparedStatement statement = connection.prepareStatement (querySQL)) {
      statement.setString (1, id);
      statement.executeUpdate ();

    }
  }
}

