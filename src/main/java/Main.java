import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class Main {
  public static void main(String[] args) {
    SQLConex sqlConex = new SQLConex ();
    Connection connection = sqlConex.obtenerConexion ();
// Ea exlplicita le gusta la letra
    try {
      CitaDAO citaDAO = new CitaDAO (connection);

      List<Cita>citas = citaDAO.obtenerTodasLasCitas ();

      System.out.println ("Lista de Citas: ");
      for (Cita cita: citas){
        System.out.println ("ID: " +  cita.getIDCita ());
        System.out.println ("Fecha: " +  cita.getFechaHora ());
        System.out.println ("Motivo: " + cita.getMotivoCita ());
        System.out.println ("Estado: "+ cita.getEstadoCita ());
        System.out.println ("IDPaciente: " +  cita.getIDPaciente ());
        System.out.println ("IDMedico: " + cita.getIDMedico ());
      }
    } catch (Exception e) {
      e.printStackTrace ();
      System.out.println ("No hay Citas: " + e.getMessage ());
    }
/*
    try {
      CitaDAO citaDAO = new CitaDAO (connection);

      Cita createCita = new Cita ();

      createCita.setIDCita ("100");
      createCita.setFechaHora ("2023-12-22 12:00:00.000");
      createCita.setMotivoCita ("Control de uñas");
      createCita.setEstadoCita ("Confirmada");
      createCita.setIDPaciente ("6");
      createCita.setIDMedico ("2");

      citaDAO.createCita (createCita);
      System.out.println ("Cita agregada correctamente...");

    } catch (Exception e) {
      e.printStackTrace ();
      System.out.println ("No se pudo crear la cita "+ e.getMessage ());
    }



    try {
      CitaDAO citaDAO = new CitaDAO(connection);

      String idCitaUpdate = "100";
      String estadoCita = "Atendido";

      Cita updateCita = new Cita();

      updateCita.setFechaHora("2023-12-22 12:00:00.000");
      updateCita.setMotivoCita("Control de astilla en el pie");
      updateCita.setEstadoCita(estadoCita);
      updateCita.setIDPaciente("6");
      updateCita.setIDMedico("2");

      citaDAO.updateCita(idCitaUpdate, updateCita);

      System.out.println("Cita " + idCitaUpdate + " se actualizó correctamente!");

    } catch (SQLException e) {
      e.printStackTrace();
      System.out.println("Error al actualizar cita: " + e.getMessage());
    }



 */

    try {
      CitaDAO citaDAO = new CitaDAO (connection);

      String idCita = "100";
      citaDAO.deleteCita (idCita);

      System.out.println ("Se elimino la cita con ID: ".concat (idCita));

    }catch (SQLException e) {
      e.printStackTrace ();
      System.out.println ("No se pudo eliminar el médico: " + e.getMessage ());
    }

  }
}
